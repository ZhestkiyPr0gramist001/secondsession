package com.example.secondsession

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class LoadingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashloading)

        Handler().postDelayed({
            startActivity(Intent(this@LoadingActivity, OnBoardingActivity::class.java))
            finish()
        }, 1000)

    }
}